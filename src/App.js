import React, { Component } from "react";
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      questionPresent: 1,
      questionLast: 5,
      talent: null,
      role: null,
      isAnimate: false,
    };
    this.myCanvasRef = React.createRef();
  }

  onChangeRadio = (event) => {
    this.setState({
      talent : event.target.value
    });
  }

  onChangeRadioContainer = (type) => {
    this.setState({
      talent : type
    });
  }

  handleNextStep = () => {
    var {questionPresent} = this.state;
    this.setState({
      isAnimate : true
    });
    setTimeout(() => { 
      this.setState({
        questionPresent : questionPresent+1,
        isAnimate : false
      });
     }, 1000);
  }

  handlePrevStep= () => {
    var {questionPresent} = this.state;
    this.setState({
      questionPresent : questionPresent-1
    });
  }

  render() {
    const { 
      questionPresent,
      questionLast,
      talent,
      role,
      isAnimate,
    } = this.state;

    return (
      <div className="content">
        <div className={
            isAnimate
            ? 
            "question-container active bounce-out" 
            : 
            questionPresent === 1 
            ?
            "question-container active"
            : questionPresent !== 1 
            ?
            "question-container"
            : null
          }
        >
          <div className="question-number">
            Question {questionPresent} / {questionLast}
          </div>
          <div className="question-title">
            What kind of talent are you looking for?
          </div>
          <div className="answers">
            <div className="answer" onClick={() => this.onChangeRadioContainer("1")}>
              <input
                name="talent"
                type="radio"
                value="1"
                className="answer-radio"
                checked={talent === "1"}
                onChange={this.onChangeRadio} />
              <div className="answer-title">
                Developers
              </div>
              <div className="answer-info">
                Game dev, Engine dev, Backend, DevOps
              </div>
            </div>
            <div className="answer" onClick={() => this.onChangeRadioContainer("2")}>
              <input
                name="talent"
                type="radio"
                value="2"
                className="answer-radio"
                checked={talent === "2"}
                onChange={this.onChangeRadio} />
              <div className="answer-title">
                Designers
              </div>
              <div className="answer-info">
                Game design, Product design, UI/UX
              </div>
            </div>
            <div className="answer" onClick={() => this.onChangeRadioContainer("3")}>
              <input
                name="talent"
                type="radio"
                value="3"
                className="answer-radio"
                checked={talent === "3"}
                onChange={this.onChangeRadio} />
              <div className="answer-title">
                Artists
              </div>
              <div className="answer-info">
                3D modelling, Illustration, Sound engineer
              </div>
            </div>
            <div className="answer" onClick={() => this.onChangeRadioContainer("4")}>
              <input
                name="talent"
                type="radio"
                value="4"
                className="answer-radio"
                checked={talent === "4"}
                onChange={this.onChangeRadio} />
              <div className="answer-title">
                Managers
              </div>
              <div className="answer-info">
                Project/Brand/Community manager
              </div>
            </div>
          </div>
          <div className="btn-container">
            <div className="previous-btn">I am not ready yet</div>
            <button className="next-btn" onClick={this.handleNextStep} disabled={talent === null ? true : false}>Next Step</button>
          </div>
        </div>
        <div className={questionPresent === 2 ? "question-container active" : "question-container"}>
          <div className="question-number">
            Question {questionPresent} / {questionLast}
          </div>
          <div className="question-title">
            What role are you looking for them to fill?
          </div>
          <div className="answers">
            <div className="answer" onClick={() => this.onChangeRadioContainer("1")}>
              <input
                name="role"
                type="radio"
                value="1"
                className="answer-radio"
                checked={role === "1"}
                onChange={this.onChangeRadio} />
              <div className="answer-title">
                Project ideas
              </div>
              <div className="answer-info">
                We need help defining the project from scratch
              </div>
            </div>
            <div className="answer" onClick={() => this.onChangeRadioContainer("2")}>
              <input
                name="role"
                type="radio"
                value="2"
                className="answer-radio"
                checked={role === "2"}
                onChange={this.onChangeRadio} />
              <div className="answer-title">
                Kickstart development
              </div>
              <div className="answer-info">
                We know what we want, just need help making it
              </div>
            </div>
            <div className="answer" onClick={() => this.onChangeRadioContainer("3")}>
              <input
                name="role"
                type="radio"
                value="3"
                className="answer-radio"
                checked={role === "3"}
                onChange={this.onChangeRadio} />
              <div className="answer-title">
                Support ongoing project
              </div>
              <div className="answer-info">
                Things are underway, just need some extra hands
              </div>
            </div>
            <div className="answer" onClick={() => this.onChangeRadioContainer("4")}>
              <input
                name="role"
                type="radio"
                value="4"
                className="answer-radio"
                checked={role === "4"}
                onChange={this.onChangeRadio} />
              <div className="answer-title">
                Something else
              </div>
              <div className="answer-info">
                None of the above
              </div>
            </div>
          </div>
          <div className="btn-container">
            <button className="previous-btn" onClick={this.handlePrevStep}>Previous Step</button>
            <button className="next-btn" onClick={this.handleNextStep} disabled={role === null ? false : true}>Next Step</button>
          </div>
        </div>
      </div>
    );
  }
}

export default App;